<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta content="HTML Tidy for HTML5 for Apple macOS version 5.6.0"
name="generator">
<title>What is Automatic Differentiation ?</title>
<link href="../general.css" rel="stylesheet" type="text/CSS">
<link href="ad.css" rel="stylesheet" type="text/CSS">
</head>
<body>
<a id="top" name="top">Automatic Differentiation (<b>A.D.</b>) is a
technique to evaluate the <b>derivatives</b> of a function defined
by a computer <b>program</b>.</a>
<hr>
<img height="7" src="../_static/redbullet.gif" width="7"> <a href=
"#howDerivative"><b>How can one obtain derivatives?</b> <img alt=
"Jump there" border="0" src=
"../_static/qbullets/scrolldown.gif"></a><br>
<img height="7" src="../_static/redbullet.gif" width="7"> <a href=
"#autoDiff"><b>Automatic Differentiation</b> <img alt="Jump there"
border="0" src="../_static/qbullets/scrolldown.gif"></a><br>
<img height="7" src="../_static/redbullet.gif" width="7"> <a href=
"#progsAndFuncs"><b>Computer Programs and Mathematical
Functions</b> <img alt="Jump there" border="0" src=
"../_static/qbullets/scrolldown.gif"></a><br>
<img height="7" src="../_static/redbullet.gif" width="7"> <a href=
"#activity"><b>Activity of variables</b> <img alt="Jump there"
border="0" src="../_static/qbullets/scrolldown.gif"></a><br>
<img height="7" src="../_static/redbullet.gif" width="7"> <a href=
"#focusForward"><b>Focus on the directional derivatives
(tangents)</b> <img alt="Jump there" border="0" src=
"../_static/qbullets/scrolldown.gif"></a><br>
<img height="7" src="../_static/redbullet.gif" width="7"> <a href=
"#focusReverse"><b>Focus on the gradients</b> <img alt="Jump there"
border="0" src="../_static/qbullets/scrolldown.gif"></a><br>
<img height="7" src="../_static/redbullet.gif" width="7"> <a href=
"#focusMulti"><b>Focus on multi-directional mode</b> <img alt=
"Jump there" border="0" src=
"../_static/qbullets/scrolldown.gif"></a><br>
<hr>
<a id="howDerivative" name="howDerivative"></a>
<h2><img src="../_static/redbullet.gif"> How can one obtain
derivatives? <a href="#top"><img alt="Jump to top" border="0" src=
"../_static/qbullets/scrollup.gif"></a></h2>
There are many ways to obtain derivatives. Maybe the most
straightforward is to go back to the <b>equations</b> that led to
the program. Suppose that some result is defined by some equations,
and the program solved these equations for this result. Then one
can write a new set of equations, whose solutions are the
derivative of the initial result. Consequently, one can write a new
program that solves these new equations for the desired
derivatives. This is mathematically very sound, but it probably is
the most <b>expensive</b> way, since it implies discretizing new
equations, then writing a new program. We all know how difficult
this is, and how many errors can be done! Moreover, in some cases
there are no simple original equations, and only the program is at
hand. Therefore we are looking for another way, more economical and
that uses only the original program.<br>
<br>
Using only the original program, one can naturally do <b>divided
differences</b>. For a given set of program's inputs,
<i><b>X</b></i>, the program <i><b>P</b></i> has computed a result
<i><b>P(X)=Y</b></i>. In the general case, both <i><b>X</b></i> and
<i><b>Y</b></i> are <b>vectors</b>, i.e. are composed of several
real numbers. Given now some normalized direction <i><b>dX</b></i>
in the space of the inputs, one can run the program <i><b>P</b></i>
again, on the new set of inputs <i><b>X+<img border="0" src=
"../_static/epsilon0.gif">dX</b></i>, where <img border="0" src=
"../_static/epsilon0.gif"> is some very small positive number. Then
a good <b>approximation</b> of the derivative is computed easily
by:<br>
<br>
<center><img src="../_static/divideddiff.gif"></center>
<br>
The <b>centered divided differences</b>, computed by:<br>
<br>
<center><img src="../_static/divideddiffcentered.gif"></center>
<br>
usually give a better approximation, but cost an additional
execution of program <i><b>P</b></i>. Of course this is just an
approximation of the derivatives. Ideally, the exact derivative is
the limit of these formulas, when <img border="0" src=
"../_static/epsilon0.gif"> tends to zero. But this makes no sense
on a real computer, since very small values of <img border="0" src=
"../_static/epsilon0.gif"> lead to truncation errors, and therefore
to erroneous derivatives. This is the main drawback of divided
differences: some tradeoff must be found between truncation errors
and approximation errors. Finding the best <img border="0" src=
"../_static/epsilon0.gif"> requires numerous executions of the
program, and even then the computed derivatives are just
approximations.<br>
<br>
<a id="autoDiff" name="autoDiff"></a>
<h2><img src="../_static/redbullet.gif"> Automatic Differentiation
: <a href="#top"><img alt="Jump to top" border="0" src=
"../_static/qbullets/scrollup.gif"></a></h2>
<b>Automatic Differentiation</b>, just like divided differences,
requires only the original program <i><b>P</b></i>. But instead of
executing <i><b>P</b></i> on different sets of inputs, it builds a
new, augmented, program <i><b>P'</b></i>, that computes the
<b>analytical</b> derivatives along with the original program. This
new program is called the <b>differentiated</b> program. Precisely,
each time the original program holds some value <i><b>v</b></i>,
the differentiated program holds an additional value
<i><b>dv</b></i>, the <b>differential</b> of <i><b>v</b></i>.
Moreover, each time the original program performs some operation,
the differentiated program performs additional operations dealing
with the differential values. For example, if the original program,
at some time during execution, executes the following instruction
on variables <tt>a</tt>, <tt>b</tt>, <tt>c</tt>, and array
<tt>T</tt>:<br>
<br>
<center><tt>a = b*T(10) + c</tt></center>
<br>
then the differentiated program must execute additional operations
on the variables and their <b>differentials</b> <tt>da</tt>,
<tt>db</tt>, <tt>dc</tt>, and array <tt>dT</tt>, that must somehow
amount to:<br>
<br>
<center><tt>da = db*T(10) + b*dT(10) + dc</tt></center>
<br>
The derivatives are now computed analytically, using the well known
formulas on derivation of elementary operations. Approximation
errors have just vanished.<br>
<br>
Again, there are two ways to implement A.D.:
<ul>
<li><b>Overloading</b> consists in telling the compiler that each
real number is replaced by a pair of real numbers, the second
holding the differential. Each elementary operation on real numbers
is overloaded, i.e. internally replaced by a new one, working on
pairs of reals, that computes the value <b>and</b> its
differential. The advantage is that the original program is
virtually unchanged, since everything is done at compile time. The
drawback is that the resulting program runs slowly because it
constantly builds and destroys pairs of real numbers. Moreover, it
is very hard to implement the "<b>reverse mode</b>" with
overloading.</li>
<li><b>Source transformation</b> consists in adding into the
program the new variables, arrays, and data structures that will
hold the derivatives, and in adding the new instructions that
compute these derivatives. The advantage is that the resulting
program can be compiled into an efficient code, and the "<b>reverse
mode</b>" is possible. The drawback is that this is an enormous
transformation, that cannot be done by hand on large programs.
Tools are needed to perform this transformation correctly and
rapidly. Our team studies this sort of tools. Our <a href=
"../tapenade/tutorial.html">Tapenade</a> engine is just one such
Automatic Differentiation tool that uses source
transformation.</li>
</ul>
In the above, we have been very vague on the notions of
<b>differentials</b> and <b>derivatives</b>. This is because there
are many "derivative objects" one may want, and each of them is
obtained through a specific Automatic Differentiation. Here are
some examples:
<ul>
<li>One may really want the <b>derivative</b> of the program. More
exactly, since the program's inputs and outputs are generally
<b>vectors</b>, the derivative is a matrix, known as the
<b>Jacobian</b> matrix, Each element <i><b>(i,j)</b></i> of the
Jacobian matrix is the partial derivative<br>
<br>
<center><img src="../_static/jacobelement.gif"></center>
<br>
of output <i><b>Y<sub>i</sub></b></i> with respect to input
<i><b>X<sub>j</sub></b></i>. For that, the derivative object that
must be computed along with each value <i><b>v</b></i> is the
<b>vector</b> of all partial derivatives of <i><b>v</b></i> with
respect to each input. When values are themselves arrays, the
derivative object becomes a matrix. This leads to carrying partial
Jacobian matrices along with the main computation stream. This is
usually expensive in time and memory space. This drawback can be
partially coped for, using sparse representations for the
derivative objects.</li>
<li>One may simply want <b>directional derivatives</b>, also known
as <b>tangents</b>. This means that, instead of the explicit
<b>Jacobian</b> matrix <i><b>J</b></i>, one just needs its effect
on a given direction vector <i><b>dX</b></i> in the input space.
This effect is actually <i><b>dY = J * dX</b></i>, but one can
evaluate it without explicitly computing <i><b>J</b></i>. For that,
the derivative object that must be computed along with each value
<i><b>v</b></i> is its "<b>differential</b>" <i><b>dv</b></i>, i.e.
(the first-order approximation of) the quantity by which
<i><b>v</b></i> varies when the input <i><b>X</b></i> varies by
<i><b>dX</b></i>. As expected, this is far less expensive than
building the whole Jacobian. Note that directional derivatives also
can provide us with the <b>Jacobian</b> matrix, if directional
deriatives are computed for each canonical direction in the input
space. In other words, this requires one computation of the
directional derivative per input variable, and even less than that,
if one knows that the Jacobian matrix is sufficiently sparse.</li>
<li>One may want <b>gradients</b>. When there is only one output,
the Jacobian matrix has only one line, which is exactly the
gradient of the result with respect to the inputs. When there are
many outputs, one defines a scalar composite result, i.e. some
linear combination of these outputs, so as to compute the gradient
of this result. To put it differently, one wants a combination of
the lines of the Jacobian <i><b>Xb<sup>*</sup> = Yb<sup>*</sup> *
J</b></i>. The "*" superscript indicates transposition, i.e.
<i><b>Xb<sup>*</sup></b></i> and <i><b>Yb<sup>*</sup></b></i> are
row vectors. This can be computed without explicitly computing
<i><b>J</b></i>. For that, the derivative object is called an
"<b>adjoint</b>", and it is computed in the reverse of the original
program's order, and will be shown later. Like with the directional
derivatives, one can compute the whole Jacobian matrix by
repeatedly computing gradients, for each canonical direction in the
output space, or fewer than that when the Jacobian is sparse. It is
easy to check that this approach is advisable when there are far
more inputs than outputs.</li>
<li>One may want <b>higher-order derivatives</b> (Hessian). This
can be done computing a derivative object which is a partial
Hessian tensor. Since this might be very expensive, another
approach is to compute <i><b>directional</b></i> higher-order
derivatives, and use them afterwards to recompute the coefficients
of the Hessian. (cf <a href=
"https://www-sop.inria.fr/tropics/tropics/documents.html#GUW97">[GriewankUtkeWalther97]</a>)</li>
<li>One may want <b>directional higher-order derivatives</b>. Given
a direction vector <b><i>dX</i></b> in the input space, one
computes along the original program a derivative object that
contains the first and higher derivatives of each intermediate
value in the direction <b><i>dX</i></b>. Higher-order derivatives
are useful to get a more accurate approximation, or to optimize
more efficiently.</li>
<li>One may want <b>Taylor series</b> expansions. (cf <a href=
"https://www-sop.inria.fr/tropics/tropics/documents.html#Gbook2000">
[Griewank00]</a>)</li>
<li>One may want to compute <b>intervals</b>. This is not strictly
speaking a matter of derivatives, but shares many similarities. The
derivative object computed along with each value <i><b>v</b></i> is
the interval into which this value may range.</li>
</ul>
Up to now, we in the Tropics team have been concerned with mainly
<b>directional derivatives</b> and <b>gradients</b>. We implemented
these two modes of Automatic Differentiation in the <a href=
"../tapenade/tutorial.html">Tapenade</a> tool, the successor of
<b>Odyssée</b>. Some years ago the former <b>SAFIR</b> team of
INRIA, who developped Odyssée, implemented also the <b>Jacobian</b>
mode to validate their research. This mode has not been maintained
in Odyssée, and therefore is not presently available in Tapenade.
The <b>TAMC</b> (now <b>TAF</b>) tool also computes <b>directional
derivatives</b> and <b>gradients</b>. The <b>ADIFOR</b> tool
computes <b>directional derivatives</b>, <b>gradients</b>, and
<b>Jacobians</b>, taking advantage of sparsity. All the above tools
are based on <b>source transformation</b>. In contrast
<b>ADOL-C</b>, based on <b>overloading</b>, has been used for
<b>directional derivatives</b> and <b>gradients</b>, but these are
relatively slow, and also for <b>directional higher-order
derivatives</b> and <b>Taylor series</b>. Other very close tools
exist, such as <b>PADRE-2</b>, mainly used for <b>error
estimation</b>. <a id="progsAndFuncs" name="progsAndFuncs"></a>
<h2><img src="../_static/redbullet.gif"> Computer Programs and
Mathematical Functions : <a href="#top"><img alt="Jump to top"
border="0" src="../_static/qbullets/scrollup.gif"></a></h2>
Before we can focus on <b>directional derivatives</b> and
<b>gradients</b>, we need to introduce a general framework in which
we can describe precisely these two modes of A.D. Essentially, we
identify <b>programs</b> with <b>sequences of instructions</b>,
themselves identified with <b>composed functions</b>.<br>
<br>
Programs contain <b>control</b>, which is essentially discrete and
therefore non-differentiable. We identify a program with the set of
all possible run-time sequences of instructions. Of course there
are <b>many</b> such sequences, and therefore we never build them
explicitly. The <b>control</b> is just how one tells the running
program to switch to one sequence or another. For example this
small program piece:
<div style="margin-left: 2em">
<pre><tt>
if (x &lt;= 1.0) then
  printf("x too small\n");
else {
  y = 1.0;
  while (y &lt;= 10.0) {
    y = y*x;
    x = x+0.5;
  }
}</tt></pre></div>
will execute according to the control as one of the following
sequences of instructions:
<pre><tt>printf("x too small\n");</tt></pre>
<pre><tt>y = 1.0;</tt></pre>
<pre><tt>y = 1.0; y = y*x; x = x+0.5;</tt></pre>
<pre>
<tt>y = 1.0; y = y*x; x = x+0.5; y = y*x; x = x+0.5;</tt></pre>
<pre>
<tt>y = 1.0; y = y*x; x = x+0.5; y = y*x; x = x+0.5; y = y*x; x = x+0.5;</tt></pre>
<i>and so on...</i>
<table width="100%">
<tr align="left">
<td></td>
</tr>
<tr align="left">
<td></td>
</tr>
<tr align="left">
<td></td>
</tr>
<tr align="left">
<td></td>
</tr>
<tr align="left">
<td></td>
</tr>
<tr align="left">
<td></td>
</tr>
</table>
<br>
<br>
Each of these sequences is "differentiable". The new program
generated by Automatic Differentiation contains of course some
control, such that each of its run-time sequences of instructions
computes the derivative of the corresponding sequence of
instructions in the initial program. Thus, this differentiated
program will probably look like (no matter whether by overloading
or by source transformation):
<div style="margin-left: 2em">
<pre><tt>
if (x &lt;= 1.0) then
  printf("x too small\n");
else {
  y = 1.0;
  dy = 0.0;
  while (y &lt;= 10.0) {
    dy = dy*x + y*dx;
    y = y*x;
    x = x+0.5;
  }
}</tt></pre></div>
But it sometimes happens, like in this example, that the control
itself depends on differentiated variables. In that case, a small
change of the initial values may result in a change of the control.
Here, a small change of <tt>x</tt> may change the number of
iterations of the <tt>while</tt> loop, and the derivative is not
defined any more. Yet the new program generated by Automatic
Differentiation will return a result, and using this derivative may
lead to errors. In Other words, the original program, with control,
is only <b>piecewise differentiable</b>, and most present A.D.
tools do not take this into account correctly. This is an open
research problem. In the meantime, we simply assume that this
problem happens rarely enough. Experience on real programs shows
that this is a reasonable assumption.<br>
<br>
Now that programs are identified with sequences of instructions,
these sequences are identified with <b>composed functions</b>.
Precisely, the sequence of instructions :<br>
<center><tt><i>I<sub>1</sub>; I<sub>2</sub>; ... I<sub>p-1</sub>;
I<sub>p</sub>;</i></tt></center>
<br>
is identified to the function :
<center><img alt="composed function" src=
"../_static/compfunc.gif"></center>
<br>
Of course each of these functions are extended to operate on the
domain of all the program variables, but variables not overwritten
by the instruction are just transmitted unchanged to the function's
result. We can then use the chain rule to formally write the
derivative of the program for a given vector input <i>x</i> :<br>
<center><img alt="chain rule" src=
"../_static/chainrule.gif"></center>
<br>
We wrote <i>f'<sub>n</sub></i> for the derivatives of function
<i>f<sub>n</sub></i>. The <i>f'<sub>n</sub></i> are thus Jacobian
matrices. To write the above in a shorter form, we introduced
<i>x<sub>n</sub></i>, which is the values of the variables just
after executing the first <i>n</i> functions (we set
<i>x<sub>0</sub></i> = <i>x</i>).<br>
<br>
Therefore, computing the derivatives is just computing and
multiplying these elementary Jacobian matrices
<i>f'<sub>n</sub>(x<sub>n-1</sub>)</i> <a id="activity" name=
"activity"></a>
<h2><img src="../_static/redbullet.gif"> Activity of variables :
<a href="#top"><img alt="Jump to top" border="0" src=
"../_static/qbullets/scrollup.gif"></a></h2>
In practice, not all the elements of the Jacobian matrix need to be
computed. Some elements can be proved to be always null, some
others are simply not needed by the end user. We can take this into
account to simplify the differentiated program. This is summarized
by the notion of <b>activity</b> of an instance of a variable. The
end-user specifies a set of output variables, for which the
derivatives are requested. These are called the <b>dependent</b>
variables. The end-user also specifies a set of input variables,
with respect to which the <b>dependent</b> output must be
differentiated. These are called the <b>independent</b> variables.
We shall say that an instance of a variable is <b>active</b> if
<ol>
<li>it depends on some independent, in a differentiable way,
and</li>
<li>some dependent depends on it, in a differentiable way.</li>
</ol>
<b>Activity</b> can be found by a static analysis. The set of the
<b>varying</b> variables, those who depend on some
<b>independent</b>, is propagated forwards through the program. At
the beginning of the program, the <b>varying</b> variables are just
initialized to the <b>independent</b>. Similarly, the set of the
<b>useful</b> variables, those whose value influence on some
<b>dependent</b>, is propagated backwards through the program. At
the end of the program, the <b>useful</b> variables are just
initialized to the <b>dependent</b>. Finally, at each point in the
program, a variable is <b>active</b> if it is both <b>varying</b>
and <b>useful</b>. Only the derivatives of <b>active</b> variables
need to be computed. <a id="focusForward" name="focusForward"></a>
<h2><img src="../_static/redbullet.gif"> Focus on the directional
derivatives (tangents): <a href="#top"><img alt="Jump to top"
border="0" src="../_static/qbullets/scrollup.gif"></a></h2>
Computing the product of all the elementary Jacobian matrices
returns the complete Jacobian matrix <i><b>J</b></i> of <i>f</i>.
This is certainly expensive in time and space. Often, one just
wants a directional derivative <i><b>dY = J * dX</b></i>. If we
replace <i><b>J =</b> f'(x)</i> by its above expansion with the
chain rule, we obtain :
<center><img alt="directional derivative" src=
"../_static/forwardmode.gif"></center>
<br>
which must be computed from right to left, because "matrix time
vector" products are so much cheaper than "matrix times matrix"
products. Things are easy here, because <i>x<sub>0</sub></i> is
required first, then <i>x<sub>1</sub></i>, and so on. Therefore
differentiated instructions, that compute the Jacobian matrices and
multiply them, can be done along with the initial program. We only
need to interleave the original instructions and the derivative
instructions. This is called the <b>tangent mode</b> of Automatic
Differentiation.<br>
<br>
In the tangent mode, the differentiated program is just a copy of
the given program, Additional derivative instructions are inserted
just before each instruction that involves <b>active</b> variables.
Each <b>active</b> variable gives birth to a new variable, of same
type and size, which is called its derivative variable. The control
structures of the program are unchanged, i.e. the Call Graph and
Flow Graph keep the same shape.<br>
<br>
Here is an example. Consider the original (piece of) program:
<div style="margin-left: 2em">
<pre><tt>
...
v2 = 2*sin(v1) + 5
if (v2&gt;0.0) then
  v4 = v2 + p1*v3/v2
  p1 = p1 + 0.5
endif
call sub1(v2,v4,p1)
...
</tt></pre></div>
Suppose that only <tt>v1, v2, v3</tt> and <tt>v4</tt> are
<b>active</b>, as defined above. Then AD introduces new
differentiated variables, that we shall write <tt><font color=
"blue">dv1, dv2, dv3</font></tt> and <tt><font color=
"blue">dv4</font></tt>. Differentiated variables retain the type
and dimension of the original variable. The tangent mode AD will
generate the corresponding piece of program, with differentiated
instructions (in <font color="blue">blue</font>) inserted just
before their original instruction:
<div style="margin-left: 2em">
<pre><tt>
...
<font color="blue">dv2 = 2*cos(v1)*dv1</font>
<font color="black">v2 = 2*sin(v1) + 5</font>
<font color="black">if (v2&gt;0.0) then</font>
<font color="blue">  dv4 = dv2*(1-p1*v3/(v2*v2)) + dv3*p1/v2</font>
<font color="black">  v4 = v2 + p1*v3/v2</font>
<font color="black">  p1 = p1 + 0.5</font>
<font color="black">endif</font>
<font color="black">call </font><font color=
"blue">sub1_d</font><font color="black">(v2,</font><font color=
"blue">dv2</font><font color="black">,v4,</font><font color=
"blue">dv4</font><font color="black">,p1)</font>
...
</tt></pre></div>
Notice that the control structure is preserved
(<tt>if-then-endif</tt>), and instructions dealing with non-active
variables are kept and not differentiated.<br>
<br>
Intrinsic functions, such as <tt>sin</tt>, are differentiated
directly, "in line", using an external configuration file which
specifies, for example, that the (partial) derivative of
<tt>sin(x)</tt> with respect to <tt>x</tt> is <tt><font color=
"blue">cos(x)</font></tt><br>
<br>
The structure of subroutine calls is also preserved, but differs
slightly: Since a routine call is in fact a shorthand for another
piece of program, and that this piece of program will be augmented
with differentiated instructions too, the call to the
differentiated routine <tt><font color="blue">sub1_d</font></tt>
simply <i>replaces</i> the call to <tt>sub1</tt>. In other words
<tt><font color="blue">sub1_d</font></tt> does the previous work of
<tt>sub1</tt> <i>plus</i> the derivatives computations. Of course,
these derivative instructions use the derivative variables, and
therefore each parameter which is active at the beginning or at the
end of the routine requires an extra parameter holding its
derivative.<br>
<br>
Consider now the special case of a call to a <b>black-box</b>
routine. This is basically the same as the FORTRAN
<tt>EXTERNAL</tt> routines but we generalize it as follows: we call
<b>black-box</b> a routine on which we cannot, or must not, or do
not want to run AD! This can be because we don't have the source
for it (<tt>EXTERNAL</tt> routines), or because we want to provide
a much better hand-written derivative routine. In this case, AD of
the calling program needs additional information about the
black-box routine. Suppose that <tt>sub1</tt> above is black-box.
If AD knows nothing about it, then it must make the conservative
assumption that <tt>p1</tt>, although not active at call time, may
become active at return time. For example, its exit value might
depend on the active input <tt>v2</tt>. Therefore AD must introduce
a derivative <tt><font color="blue">dp1</font></tt>, initialized to
<tt>0.0</tt> before the call, and containing some nontrivial result
upon return. The last lines of the differentiated code should be:
<div style="margin-left: 2em">
<pre><tt>
...
<font color="blue">dp1 = 0.0</font>
<font color="black">call </font><font color=
"blue">sub1_d</font><font color="black">(v2,</font><font color=
"blue">dv2</font><font color="black">,v4,</font><font color=
"blue">dv4</font><font color="black">,p1,<font color=
"blue">dp1</font><font color="black">)</font>
...
</font></tt></pre></div>
<font color="black">If the user knows that this is not the case,
there must be a way to tell the tool. For example in Tapenade, one
can give the "dependences" of each output with respect to each
input. The Tapenade FAQ has a section that explains <a href=
"../tapenade/faq.html#Libs1">how to give information on
<b>black-box</b> routines</a>. For example, the following matrix
indicates that the exit <tt>p1</tt> does not depend on any active
entry variable.<br>
<br></font>
<center><font color="black"><img alt="dependence matrix of sub1"
src="../_static/depmatrix.gif"></font></center>
<font color="black"><br>
This matrix can capture precise behavior. For example it also
implies here that although the entry active <tt>v2</tt> is used to
compute the exit, active, <tt>v4</tt>, the exit <tt>v2</tt> is not
active any more, because it only depends on the input <tt>p1</tt>,
which is not active. When AD is provided with this dependence
matrix for black-box <tt>sub1</tt> (e.g. in an external
configuration file), it generates a program that calls</font>
<div style="margin-left: 2em">
<pre><font color="black"><tt>
<font color="black">call </font><font color=
"blue">sub1_d</font><font color="black">(v2,</font><font color=
"blue">dv2</font><font color="black">,v4,</font><font color=
"blue">dv4</font><font color="black">,p1)</font>
</tt></font></pre></div>
<font color="black">If the user then provides a hand-made
<tt><font color="blue">sub1_d</font></tt>, with correct arguments
at the correct places, everything will run fine!<br>
<br>
+ non-specialization principle for multiple routine calls +
specifics of commons, data structures non-specialization
principle.<br>
<br>
This is all we will say here about the general principles of AD in
direct mode. To go further, you may read the <a href=
"https://hal.archives-ouvertes.fr/hal-00913983">Tapenade reference
article</a> and the <a href=
"https://hal.archives-ouvertes.fr/inria-00069880">Tapenade 2.1
users guide</a>, that will describe conventions about variable
names and declarations, differentiation of individual instructions
and control structures, and in general all you must know to
understand and use differentiated programs produced by
Tapenade.<br>
<a id="focusReverse" name="focusReverse"></a></font>
<h2><font color="black"><img src="../_static/redbullet.gif"> Focus
on the gradients : <a href="#top"><img alt="Jump to top" border="0"
src="../_static/qbullets/scrollup.gif"></a></font></h2>
<font color="black">Whereas the tangent mode of AD is maybe the
most immediate, it turns out that many applications indeed need
<b>gradients</b>. The reverse mode of AD does just that! In fact,
the reverse mode computes gradients in an extremenly efficient way,
at least theoretically. This is why it is one of the most promising
modes of AD.<br>
<br>
Strictly speaking, the gradient of a function <i>f</i> is defined
only when <i>f</i> returns a scalar. In other words, there can be
only one optimization criterion. If <i>f</i> returns a vector
<b>Y</b> of results, we must define from <b>Y</b> a scalar
composite result, i.e. a linear combination of its elements. We
write this scalar optimization criterion as a dot product</font>
<center><font color="black"><img alt="Y_bar^* . Y" src=
"../_static/dotgradient.gif"></font></center>
<font color="black">where <img src="../_static/ybar.gif"> is
nothing but the vector of the weights of each component of
<b>Y</b>. The gradient of this scalar optimization criterion is
therefore, after transposition:</font>
<center><font color="black"><img alt="gradient" src=
"../_static/reversemode.gif"></font></center>
<font color="black"><br>
Again, this is most efficiently computed from right to left,
because matrix-times-vector products are so much cheaper than
matrix-times-matrix products. This is the principle of the reverse
mode.<br>
<br>
This turns out to make a very efficient program, at least
theoretically (cf Section 3.4 of <a href=
"https://www-sop.inria.fr/tropics/tropics/documents.html#Gbook2000">
[Griewank00]</a>). The computation time required for the gradient
is only a small multiple of the run time of the original program.
It is independent from the number of inputs in <b>X</b>. Moreover,
if there are only a small number <i>m</i> of scalar outputs in
<b>Y</b>, one can compute the whole Jacobian matrix row by row very
efficiently.<br>
<br>
However, we observe that the <b>X</b><sub><i>n</i></sub> are
required in the <b>inverse</b> of their computation order. If the
original program <b>overwrites</b> a part of
<b>X</b><sub><i>n</i></sub>, the differentiated program must
restore it before it is used by
<i>f'<sub>n+1</sub></i>(<b>X</b><sub><i>n</i></sub>).</font>
<center><font color="black"><b><font color="red">This is the
biggest problem of the reverse mode !!</font></b></font></center>
<font color="black">But the potential advantages of the reverse
mode are so promising that we are constantly working on how to
overcome this problem... There are two strategies to solve
it:</font>
<ul>
<li><font color="black"><b>Recompute All (RA):</b> the
<b>X</b><sub><i>n</i></sub> is recomputed when needed, restarting
the program on input <b>X</b><sub><i>0</i></sub> until instruction
<i><tt>I</tt><sub>n</sub></i>. The TAF tool uses this strategy. The
following figure illustrates the <b>RA</b> strategy:</font>
<center><font color="black"><img alt="Recompute-All strategy" src=
"../_static/ra.gif"></font></center>
<font color="black">One can see that before the adjoint of each
instruction, the necessary part of the original program is
recomputed. Notice that this requires that we save the environment
before the rirst run (big black dot), and restore it (big white
dots) before each duplicate run. We see that brute-force <b>RA</b>
strategy has a quadratic time cost with respect to the total number
of run-time instructions <i>p</i>.</font></li>
<li style="list-style: none"><font color="black"><br></font></li>
<li><font color="black"><b>Store All (SA):</b> the
<b>X</b><sub><i>n</i></sub> are restored from a stack when needed.
This stack is filled during a preliminary run of the program, the
<b>"forward sweep"</b>, that additionally stores variables on the
stack just before they are overwritten. The tools Adifor and
Tapenade use this strategy. The following figure illustrates the
<b>SA</b> strategy:</font>
<center><font color="black"><img alt="Store-All strategy" src=
"../_static/sa.gif"></font></center>
<font color="black">One can see that before each instruction of the
forward sweep, some values are saved, (small black dots) and they
are restored (small white dots) just before their corresponding
derivative instruction during the so-called <b>backward sweep</b>.
Brute-force <b>SA</b> strategy has a linear memory cost with
respect to <i>p</i>.</font></li>
</ul>
<font color="black">Both <b>RA</b> and <b>SA</b> strategies need a
special storage/recomputation trade-off in order to be really
profitable, and this makes them become very similar. This trade-off
is called <b>checkpointing</b>. Checkpointing consists in selecting
some part of the program, and in <b>not</b> storing the values
overwritten by this part (in <b>SA</b> mode) nor repeating
execution of this part (in <b>RA</b> mode), at the cost of some
extra memorization of the complete state of the program, that we
call a <b>snapshot</b>.</font>
<ul>
<li><font color="black">In <b>Recompute All (RA)</b> mode, the
result of checkpointing part <b>P</b> is shown on the top of next
figure. The <b>snapshot</b> (big black dot) memorizes the state of
the program just after <b>P</b>.</font>
<center><font color="black"><img alt="Recompute-All checkpoint"
src="../_static/rackp.gif"></font></center>
<font color="black">The bottom part of the figure shows the
execution of the program when there are many nested checkpoints.
There are fewer duplicate executions, but it costs some extra
snapshots.</font></li>
<li style="list-style: none"><font color="black"><br></font></li>
<li><font color="black">In <b>Store All (SA)</b> mode, the result
of checkpointing part <b>P</b> is shown on the top of next figure.
The <b>snapshot</b> (big black dot) memorizes the state of the
program just before <b>P</b>. There is an additional cost, which is
the duplicate execution of <b>P</b>.</font>
<center><font color="black"><img alt="Store-All checkpoint" src=
"../_static/sackp.gif"></font></center>
<font color="black">The bottom part of the figure shows the
execution of the program when there are many nested checkpoints.
There are fewer values memorized, but it costs some snapshots and
some duplicate executions.</font></li>
</ul>
<font color="black">For instance in Tapenade, the strategy is
<b>Store All (SA)</b>, and the nested checkpointed parts are the
subroutine calls. In the model case where the program is a sequence
of <i><b>N</b></i> parts, all of them with grossly the same cost.
it was shown that the cost of the reverse differentiated program,
in terms of memory size and in terms of number of duplicated
executions, grow only like the logarithm of <i><b>N</b></i>, which
is very good.<br>
<br>
<b>In the sequel, we shall concentrate on the Store All (SA)
strategy.</b><br>
<br>
The structure of a reverse differentiated program is therefore more
complex than in the tangent mode. There is first a <b>forward
sweep</b>, identical to the original program augmented with
instructions that memorize the intermediate values before they are
overwritten, and instructions that memorize the control flow in
order to reproduce the control flow in reverse during the
<b>backward sweep</b>. Then starts the <b>backward sweep</b>, that
actually computes the derivatives, and uses the memorized values
both to compute the derivatives of each instruction and to choose
its flow of control. Here is the same example as for the tangent
mode. Consider again this original (piece of) program:</font>
<div style="margin-left: 2em">
<pre><font color="black"><tt>
...
v2 = 2*sin(v1) + 5
if (v2&gt;0.0) then
  v4 = v2 + p1*v3/v2
  p1 = p1 + 0.5
endif
call sub1(v2,v4,p1)
...
</tt></font></pre></div>
<font color="black">Suppose that only <tt>v1, v2, v3</tt> and
<tt>v4</tt> are <b>active</b>, as defined above. Then AD introduces
new differentiated variables, that we shall write <tt><font color=
"blue">v1b, v2b, v3b</font></tt> and <tt><font color=
"blue">v4b</font></tt>. Here also, differentiated variables retain
the type and dimension of the original variable. Suppose that
memorizations are done with a stack, used through procedures
<tt>push</tt> and <tt>pop</tt>. The <b>forward sweep</b> will look
like:</font>
<div style="margin-left: 2em">
<pre><font color="black"><tt>
...
<font color="blue">push(v2)</font>
v2 = 2*sin(v1) + 5
if (v2&gt;0.0) then
  <font color="blue">push(v4)</font>
  v4 = v2 + p1*v3/v2
  <font color="blue">push(p1)</font>
  p1 = p1 + 0.5
  <font color="blue">push(true)</font>
else
  <font color="blue">push(false)</font>
endif
<font color="blue">push(v2)</font>
<font color="blue">push(v4)</font>
<font color="blue">push(p1)</font>
call sub1(v2,v4,p1)
...
</tt></font></pre></div>
<font color="black">The isolated <tt>push</tt> are memorizations of
intermediate values and control. The group of three <tt>push</tt>
before the subroutine call is a <b>snapshot</b> because the call
will be <b>checkpointed</b>. The corresponding <b>backward
sweep</b> will then be:</font>
<div style="margin-left: 2em">
<pre><font color="black"><tt>
...
<font color="blue">pop(p1)</font>
<font color="blue">pop(v4)</font>
<font color="blue">pop(v2)</font>
call sub1_b(v2,v2b,v4,v4b,p1)
<font color="blue">pop(control)</font>
if (control) then
  <font color="blue">pop(p1)</font>
  <font color="blue">pop(v4)</font>
  v3b = v3b + v4b*p1/v2
  v2b = v2b + v4b*(1-p1*v3/(v2*v2))
  v4b = 0.0
endif
<font color="blue">pop(v2)</font>
v1b = v1b + v2b*2*cos(v1)
v2b = 0.0
...
</tt></font></pre></div>
<font color="black">Notice that the adjoint of one original
instruction is now very often a sequence of instructions. Notice
also that subroutine <tt>sub1_b</tt> is the result of checkpointing
on <tt>sub1</tt>, and therefore itself consists of a <b>forward
sweep</b> followed by a <b>backward sweep</b>. On the other hand,
intrinsic functions are better differentiated in line, like here
where there is no call to a <tt>sin_b</tt> procedure, but rather an
explicit use of the partioal derivative <tt>cos(v1)</tt>.<br>
<br>
The additional parameters passed to the call to <tt>sub1_b</tt>
strongly depend on the results of <b>activity analysis</b>. If an
argument is not active at the entry into the procedure nor at the
exit from the procedure, then we need not pass a derivative
argument. Otherwise we must pass one, and it must be initialized
when necessary.<br>
<br>
When the procedure is a function, then its derivative procedure may
have an extra derivative parameter, which is the differential of
the function's output. On the other hand, the function's output
itself can very well be forgotten: remember the call to the
derivative function are enclosed in a <b>backward sweep</b>, so
there is no one left there to use this function's result!<br>
<br>
This is all we will say here about the general principles of AD in
reverse mode. To go further, you may read the <a href=
"https://hal.archives-ouvertes.fr/hal-00913983">Tapenade reference
article</a> and the <a href=
"https://hal.archives-ouvertes.fr/inria-00069880">Tapenade 2.1
user's guide</a>, that will describe conventions about variable
names and declarations, differentiation of individual instructions,
control structures, procedure calls, and in general all you must
know to understand and use differentiated programs produced by
Tapenade.<br>
<br>
<a id="focusMulti" name="focusMulti"></a></font>
<h2><font color="black"><img src="../_static/redbullet.gif"> Focus
on multi-directional mode : <a href="#top"><img alt="Jump to top"
border="0" src="../_static/qbullets/scrollup.gif"></a></font></h2>
<font color="black"><br>
...<br>
<br></font>
</body>
</html>
