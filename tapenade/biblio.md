Bibliography
============

The Tapenade Automatic Differentiation tool: principles, model, and specification 
[RR-7957](https://hal.inria.fr/hal-00695839),
[ACM Transactions on Mathematical Software](https://hal.inria.fr/hal-00913983)

[Mixed-language automatic differentiation](https://hal.inria.fr/hal-01852216)

[Native handling of Message-Passing communication in Data-Flow analysis ](http://www-sop.inria.fr/tropics/papers/PascualHascoet12.html)

[Tapenade for C](http://www-sop.inria.fr/tropics/papers/PascualHascoet08.html)

[Extension of TAPENADE towards Fortran 95](http://www-sop.inria.fr/tropics/papers/Pascual2005EoT.html)

[TBR Analysis in Reverse-Mode Automatic Differentiation](https://hal.inria.fr/inria-00071727)

[TAPENADE 2.1 user's guide](https://hal.inria.fr/inria-00069880)

[Tapenade (hal.inria.fr/hal-02975414)](https://hal.inria.fr/hal-02975414)
