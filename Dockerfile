FROM sphinxdoc/sphinx

WORKDIR /docs
ADD requirements.txt /docs
RUN pip3 install -r requirements.txt
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git
