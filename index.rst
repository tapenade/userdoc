Tapenade User Documentation
===========================

.. toctree::
   :maxdepth: 2

   tapenade/whatisad
   tapenade/tutorial
   tapenade/faq
   tapenade/diffversions
   tapenade/biblio
   download
   AUTHORS

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. sectionauthor:: tapenade@inria.fr

.. raw:: html
    :file: build/html/gittagdoc.html
