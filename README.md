How to build Tapenade User Documentation
========================================

After `git push`, artifact of userdoc is built in:  
`https://tapenade.gitlabpages.inria.fr/userdoc`  
cf `.gitlab-ci.yml`.

It uses `registry.gitlab.inria.fr/tapenade/tapenade/doc` docker image built
from `Dockerfile`:  
`docker build -t registry.gitlab.inria.fr/tapenade/tapenade/doc:latest .`  
`docker run --rm -v ${PWD}:/docs registry.gitlab.inria.fr/tapenade/tapenade/doc make html`  
On linux, add -u option:  
`docker run -u $(stat -c "%u:%g" ./) --rm -v ${PWD}:/docs registry.gitlab.inria.fr/tapenade/tapenade/doc make html`
